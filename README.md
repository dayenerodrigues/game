# Cannon Lord

Este jogo foi desenvolvido com base neste exemplo: http://www.bpixel.com.br/cursos/cannonlord. O jogo tem por objetivo matar o maior número de inimigos possível antes que as vidas do canhão acabem. O canhão tem 3 vidas, perdendo uma a cada inimigo que passa para o final da tela. 


# Execução

O jogo está disponível no Itch.io (https://dayenerodrigues.itch.io/cannon-lord) e também existem builds referentes à aplicação Web e Android nas respectivas pastas Web Build e Android Build. 

### Web Build

É possível executar o jogo diretamente de seu arquivo index.html utilizando o navegador Firefox. Ou acessando a página do jogo no Itch.io (https://dayenerodrigues.itch.io/cannon-lord)

### Android Build

É possível executar o jogo transferindo a .apk do mesmo para o celular e instalando a aplicação no smartphone android.  

# Organização

Foi criado um **Base Controller**, que contém as ações padrão dos elementos que derivam dele. Foi também criado um **Game Manager**, usado para manipular comportamentos do jogo em geral. Além disso, foi implementado o **Game State** para guiar o jogador de acordo com cada estado de jogo. 

# Como jogar

O jogo é totalmente controlável pelo mouse. O jogador deve clicar em qualquer lugar da tela para atirar. O botão "Pause", localizado o canto superior direito da tela, dá ao jogador a possibilidade de pausar o jogo e exibe um novo botão, "Reiniciar",  caso o jogador deseje começar novamente. O jogador pode ver sua pontuação e quantidade de vidas restantes no canto superior esquerdo da tela.

# Observações

O jogo está funcionando no dispositivo Android, completamente funcional. Porém, acontece um problema de resolução.