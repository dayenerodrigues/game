﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseController : MonoBehaviour {

    public GameManager gamemanager;

    private void Awake()
    {
        gamemanager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    protected virtual void Destruir()
    {
        gameObject.SetActive(false);
    }

    protected virtual void onDisabled()
    {
        CancelInvoke();
    }

    protected virtual void OnBecameInvisible()
    {
        Invoke("Destruir", 0);
    }
}
