﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : BaseController{

    private float modfLado = 0;
    public Rigidbody2D objTiro;

    void Update()
    {
        if (transform.rotation.z >= 0.65f)
            modfLado = 1;


        if (transform.rotation.z <= -0.65f)
            modfLado = 0;


        if (modfLado.Equals(0))
            transform.Rotate(new Vector3(0, 0, 50) * Time.deltaTime);
        else
            transform.Rotate(new Vector3(0, 0, -50) * Time.deltaTime);
        
        if (Input.GetMouseButtonDown(0))
            Invoke("Atirar", 0);

        else if (Input.GetMouseButtonUp(0))
            CancelInvoke("Atirar");
    }

    void Atirar()
    {
        if (gamemanager.gamestate.Equals(GameState.EM_JOGO))
        {
            var tiro = Instantiate(objTiro, transform.position, transform.rotation);
            tiro.AddForce(transform.up * 800);
        }
    }
}
