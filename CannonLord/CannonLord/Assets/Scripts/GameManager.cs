﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    private float intervaloMorcego;
    public float tempo;
    private float tempoAuxMorcego;
    private float tempoAuxFrequencia;
    private int limiteMorcego;
    public int vida;
    public int pontos;
    public GameObject objInimigo;
    public List<GameObject> listaInimigos = new List<GameObject>();
    public GameState gamestate;
    public Text lblPontos;
    public Text lblVidas;
    public Text lblFimJogo;
    public Button btnPause;
    public Button btnReiniciar;
    
    private void Start()
    {
        btnReiniciar.gameObject.SetActive(false);
        lblFimJogo.gameObject.SetActive(false);
        gamestate = GameState.EM_JOGO;
        vida = 3;
        pontos = 0;
        tempo = 0f;
        tempoAuxMorcego = 0f;
        tempoAuxFrequencia = 0f;
        intervaloMorcego = 1.5f;
        limiteMorcego = 10;
        GameObject novoMorcego = Instantiate(objInimigo) as GameObject;
        listaInimigos.Add(novoMorcego);
        AtualizaUI();
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.R)) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        if (gamestate.Equals(GameState.EM_JOGO))
        {
            tempo += Time.deltaTime;

            if (tempo - tempoAuxFrequencia >= 10f && intervaloMorcego > 0.5) {
                intervaloMorcego -= 0.1f;
                limiteMorcego += 5;
                tempoAuxFrequencia = tempo;
                Debug.Log(tempo);
            }


            if (tempo - tempoAuxMorcego >= intervaloMorcego && listaInimigos.Count < limiteMorcego)
            {
                GameObject novoMorcego = Instantiate(objInimigo) as GameObject;
                listaInimigos.Add(novoMorcego);
                tempoAuxMorcego = tempo;
            }

            if (vida == 0)
            {
                gamestate = GameState.FIM_JOGO;
            }
        }

        verificaGameState();
        AtualizaUI();
    }

    private void verificaGameState() {
        if (gamestate.Equals(GameState.PAUSADO) && Time.timeScale != 0)
        {
            btnReiniciar.gameObject.SetActive(true);
            Time.timeScale = 0;
        } else if (gamestate.Equals(GameState.EM_JOGO) && Time.timeScale == 0)
        {
            Time.timeScale = 1;
            btnReiniciar.gameObject.SetActive(false);
        } else if (gamestate.Equals(GameState.FIM_JOGO)) {
            Time.timeScale = 0;
            btnReiniciar.gameObject.SetActive(true);
            lblFimJogo.gameObject.SetActive(true);
        }
    }

    public void Click() {
        Debug.Log("click");
        if (gamestate.Equals(GameState.EM_JOGO))
        {
            gamestate = GameState.PAUSADO;
            btnPause = GetComponent<Button>();
            btnPause.image.sprite = Resources.Load("Assets/btn_play") as Sprite;
        }
        else if (gamestate.Equals(GameState.PAUSADO))
        {
            gamestate = GameState.EM_JOGO;
            btnPause = GetComponent<Button>();
            btnPause.image.sprite = Resources.Load("Assets/btn_pause") as Sprite;
        }       
    }

    void AtualizaUI() {
        lblPontos.text = "Pontos: " + pontos.ToString();
        lblVidas.text = "Vidas: " + vida.ToString();
    }

    public void ReiniciaJogo() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    
}
