﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiroController : BaseController {
        
    private void OnCollisionEnter2D(Collision2D collision)
    {       
        if (collision.gameObject.name.StartsWith("rocha")){
            Invoke("Destruir", 0);
        }

        if (collision.gameObject.name.StartsWith("morcego"))
        {
            gamemanager.pontos += 10;
            Invoke("Destruir", 0);
        }
    }
}
