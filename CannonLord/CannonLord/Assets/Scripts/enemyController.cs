﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : BaseController {

    public float velocidadeY;
    private float velocidadeX;
    private float tempoAux;
    private int modfLado;
    public GameObject morcego;

	void Start () {
        morcego = this.gameObject;
        morcego.transform.position = new Vector2(Random.Range(50f, 210f), 320f);
        modfLado = Random.Range(0, 1) == 0 ? -1 : 1;

        if (gamemanager.tempo > 10f)
            velocidadeY *= gamemanager.tempo / 10f;

        velocidadeX = 3*velocidadeY;
        tempoAux = 0f;

        Physics2D.IgnoreLayerCollision(8, 9);
        Physics2D.IgnoreLayerCollision(9, 9);
    }

    void Update()
    {
        if (transform.position.x <= 50f)
            modfLado = 1;
        
        else if (transform.position.x >= 210f)
            modfLado = -1;

        if (gamemanager.tempo - tempoAux >= 10f)
        {
            velocidadeX += 1.5f;
            velocidadeY += 0.5f;
            tempoAux = gamemanager.tempo;
        }

        morcego.GetComponent<Rigidbody2D>().velocity = new Vector2(modfLado * velocidadeX, -velocidadeY);
    }

    protected override void Destruir()
    {
        gamemanager.listaInimigos.Remove(morcego.gameObject);
        
        base.Destruir();
    }

    protected override void OnBecameInvisible()
    {
        if (morcego.transform.position.y <= -10f)
        {
            gamemanager.vida -= 1;
        }
        base.OnBecameInvisible();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name.StartsWith("tiro"))
        {
            Invoke("Destruir", 0);
        }
    }

}
